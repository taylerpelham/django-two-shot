from django.forms import ModelForm
from .models import Receipt, ExpenseCategory, Account

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'number']


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ['name']


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']
